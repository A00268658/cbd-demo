package cbddemo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("hello")
public class HelloWorldServelet{
	@GET
	@Produces("text/plain")
	public String getText() {
		return new HelloWorld().printMyString();
	}
}