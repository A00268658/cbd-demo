package cbddemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.testng.annotations.Test;

public class HelloWorldTest {
	@Test
    void justAnExample() {
		assertEquals("Hello World!", new HelloWorld().printMyString());
    }
}
