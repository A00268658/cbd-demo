package selenium.steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Hooks {
	public static WebDriver driver;
	
	@Before
	public void i_am_on_a_web_browser() throws Throwable {
		WebDriverManager.chromedriver().version("81.0.4044.129").setup();
		driver = new ChromeDriver();
	}
	
	@After
	public void tearDown() {
		driver.close();
	}
}
