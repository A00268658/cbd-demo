package selenium.steps;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class HelloWorldSteps {
	WebDriver driver = Hooks.driver;
	
	@Given("^I am on the test page$")
	public void i_am_logged_in() throws InterruptedException {
		driver.get("http://localhost:8080/cbddemo/rest/hello");
	}
	
	@Then("I should see Hello World")
	public void i_should_see_hello_world() throws InterruptedException {
		String actual = driver.findElement(By.xpath("/html/body/pre")).getText();
		assertEquals("Hello World", actual);
	}

}
